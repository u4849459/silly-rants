Comprehending List Comprehensions
---

```haskell
primes :: [Integer]
primes [ x | x <- [2..], all (mod x . (/= 0)) [2..x-1]]
```
